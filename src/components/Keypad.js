import React, { Component } from "react";

export class keypad extends Component {
  state = {
    title: "",
  };

  numberOperator = (e) => {
    this.setState({
      title: this.state.title + e.target.innerHTML,
    });
  };

  delete = (e) => {
    this.setState({
      title: "",
    });
  };

  result = (e) => {
    try {
      this.setState({
        title: Function('"use strict";return (' + this.state.title + ")")(),
      });
    } catch {
      this.setState({
        title: "",
      });
    }
  };

  render() {
    return (
      <>
        <input
          type="text"
          className="result"
          value={this.state.title}
          style={headerStyle}
        ></input>
        <div className="keypad" style={keypadStyle}>
          <button className="Refresh" onClick={this.delete}>
            Ac
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            (
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            )
          </button>
          <button
            className="numberOperator operator"
            onClick={this.numberOperator}
          >
            /
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            1
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            2
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            3
          </button>
          <button
            className="numberOperator operator"
            onClick={this.numberOperator}
          >
            *
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            4
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            5
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            6
          </button>
          <button
            className="numberOperator operator"
            onClick={this.numberOperator}
          >
            -
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            7
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            8
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            9
          </button>
          <button
            className="numberOperator operator"
            onClick={this.numberOperator}
          >
            +
          </button>
          <button
            className="numberOperator"
            style={{ gridColumn: "span 2" }}
            onClick={this.numberOperator}
          >
            0
          </button>
          <button className="numberOperator" onClick={this.numberOperator}>
            .
          </button>
          <button className="equalto operator" onClick={this.result}>
            =
          </button>
        </div>
      </>
    );
  }
}
const headerStyle = {
  backgroundColor: "grey",
  border: "0",
  outline: "none",
  width: "100vw",
  height: "20vh",
  color: "white",
  textAlign: "right",
  fontSize: "6rem",
  cursor: "pointer",
};

const keypadStyle = {
  width: "100vw",
  height: "80vh",
  display: "grid",
  gridTemplateColumns: "1fr 1fr 1fr 1fr",
  fontSize: "4em",
  fontWeight: "bold",
};

export default keypad;
